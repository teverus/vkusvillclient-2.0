from dataclasses import dataclass


@dataclass
class FoodCard:
    name: str = None
    weight: int = None
    price: int = None
    ratio: float = None
    url: str = None
    amount: int = None
    availability: str = None
    state: str = None
