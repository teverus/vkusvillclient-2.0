from PyQt5.QtWidgets import QPushButton

from Code.Widgets.BaseWidgets.BaseWidget import BaseWidget, Font


class Button(QPushButton, BaseWidget):
    def __init__(
        self,
        parent,
        width,
        height,
        padding_x=0,
        padding_y=0,
        right_from=None,
        under=None,
        text=None,
        align=None,
        font=Font.REGULAR,
        icon=None,
        icon_size=36,
        style=None,
        clicked=None,
        visible=True,
        disabled=False,
    ):
        super().__init__(
            parent,
            width,
            height,
            padding_x=padding_x,
            padding_y=padding_y,
            right_from=right_from,
            under=under,
            text=text,
            font=font,
            icon=icon,
            icon_size=icon_size,
            style=style,
            clicked=clicked,
            visible=visible,
            disabled=disabled,
            align=align,
        )
