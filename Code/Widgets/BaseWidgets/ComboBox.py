from PyQt5.QtWidgets import QComboBox

from Code.Widgets.BaseWidgets.BaseWidget import BaseWidget


class ComboBox(QComboBox, BaseWidget):
    def __init__(
        self,
        parent,
        width,
        height,
        items,
        padding_x=0,
        padding_y=0,
        right_from=None,
        under=None,
        text=None,
        style=None,
        current_text_changed=None,
        default_index=None,
        disabled=False,
    ):
        super().__init__(
            parent,
            width,
            height,
            right_from=right_from,
            under=under,
            padding_x=padding_x,
            padding_y=padding_y,
            text=text,
            style=style,
            disabled=disabled,
        )

        self.addItems(items)

        if current_text_changed:
            self.currentTextChanged.connect(current_text_changed)

        if default_index:
            self.setCurrentIndex(default_index)
