from PyQt5.QtWidgets import QWidget

from Code.Widgets.BaseWidgets.Label import Label
from Code.constants import WINDOW_WIDTH, WINDOW_HEIGHT, BASE_GREY


class Loader(QWidget):
    def __init__(self, parent, percentage="", message=""):
        super().__init__()
        self.percentage = percentage
        self.message = message

        self.background = Label(
            parent,
            WINDOW_WIDTH,
            WINDOW_HEIGHT,
            text=f"{str(self.percentage)}\n{self.message}",
            style=BASE_GREY,
            visible=False,
        )

    def set_visible(self, status):
        self.background.setVisible(status)

    # --- Percentage methods -----------------------------------------------------------
    def set_percentage(self, new_percentage):
        _, message = self.background.text().split("\n")
        new_line = f"{new_percentage} %\n{message}"
        self.background.setText(new_line)

    def disable_percentage(self):
        _, message = self.background.text().split("\n")
        new_line = f"\n{message}"
        self.background.setText(new_line)

    # --- Message methods --------------------------------------------------------------
    def set_message(self, new_message):
        percentage, _ = self.background.text().split("\n")
        new_line = f"{percentage}\n{new_message}"
        self.background.setText(new_line)
