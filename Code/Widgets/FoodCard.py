import webbrowser

from Code.Widgets.BaseWidgets.BaseWidget import Font
from Code.Widgets.BaseWidgets.Button import Button
from Code.Widgets.BaseWidgets.Image import Image
from Code.Widgets.BaseWidgets.Label import Label
from Code.Workers.ChangeFoodStatusWorker import ChangeFoodStatusWorker
from Code.constants import (
    PADDING_SMALL,
    Color,
    BUTTON_HEIGHT,
    IMAGE_WIDTH,
    Icon,
    FoodCardState,
)


# noinspection PyAttributeOutsideInit
class FoodCard:
    def __init__(
        self,
        parent,
        image_width=IMAGE_WIDTH,
        image_height=int(BUTTON_HEIGHT * 6) - 4,
        path_to_image="Files/Images/a_stub.png",
        # Position
        right_from=None,
        under=None,
        move_to=None,
        padding_x=0,
        padding_y=0,
    ):
        super().__init__()

        # --- Widget characteristics ---------------------------------------------------
        self.parent = parent
        self.image_width = image_width
        self.image_height = image_height
        self.path_to_image = path_to_image
        self.right_from = right_from
        self.under = under
        self.move_to = move_to
        self.padding_x = padding_x
        self.padding_y = padding_y

        # --- Variables ----------------------------------------------------------------
        self.url = None

        # --- Widgets ------------------------------------------------------------------
        self.image = self.get_image()

        self.blocked_button = self.get_block_button()
        self.question_button = self.get_question_button()
        self.good_button = self.get_good_button()
        self.checked_button = self.get_checked_button()

        self.ratio_label = self.get_ratio_label()
        self.weight_label = self.get_weight_label()
        self.price_label = self.get_price_label()
        self.availability_and_amount_label = self.get_availability_and_amount_label()

        self.title_button = self.get_title_button()

        self.stub = self.get_stub()

    # === WIDGETS ======================================================================

    @property
    def button_height(self):
        return int((self.image_height - self.image.height()) / 3) + 1

    @property
    def button_quarter(self):
        return int(self.image_width / 4)

    @property
    def button_three_quarters(self):
        return int(self.image_width * 0.5)

    def get_image(self):
        result = Image(
            self.parent,
            self.path_to_image,
            scale_to_width=self.image_width,
            right_from=self.right_from,
            under=self.under,
            move_to=self.move_to,
            padding_x=self.padding_x,
            padding_y=self.padding_y,
        )

        return result

    def get_block_button(self):
        result = Button(
            self.parent,
            width=self.button_quarter,
            height=self.button_height,
            under=self.image,
            icon=Icon.THUMBS_DOWN,
            clicked=self.__set_blocked_status,
        )

        return result

    def get_good_button(self):
        result = Button(
            self.parent,
            width=self.button_quarter,
            height=self.button_height,
            right_from=self.question_button,
            icon=Icon.THUMBS_UP,
            clicked=self.__set_good_status,
        )

        return result

    def get_question_button(self):
        result = Button(
            self.parent,
            width=self.button_quarter,
            height=self.button_height,
            right_from=self.blocked_button,
            icon=Icon.QUESTION,
            clicked=self.__set_question_status,
        )

        return result

    def get_checked_button(self):
        result = Button(
            self.parent,
            width=self.button_quarter,
            height=self.button_height,
            right_from=self.good_button,
            icon=Icon.CHECKED,
            clicked=self.__set_checked_status,
        )

        return result

    def get_title_button(self):
        result = Button(
            self.parent,
            width=self.image_width,
            height=int(self.button_height * 2) - PADDING_SMALL + 2,
            under=self.blocked_button,
            text="Lorem Ipsum",
            font=Font.REGULAR_BOLD,
            style="background: LightGreen",
            clicked=self.__open_food_online,
        )

        return result

    def get_ratio_label(self):
        result = Label(
            self.parent,
            width=self.button_quarter,
            height=self.button_height,
            right_from=self.image,
            padding_x=-self.button_quarter,
            text="[0]",
            style=f"background: {Color.TRANS_GREEN}",
        )

        return result

    def get_weight_label(self):
        result = Label(
            self.parent,
            width=self.button_quarter,
            height=self.button_height,
            under=self.image,
            padding_y=-self.button_height,
            text="0 г.",
            style=f"background: {Color.TRANS_WHITE}",
        )

        return result

    def get_price_label(self):
        result = Label(
            self.parent,
            width=self.button_quarter,
            height=self.button_height,
            move_to=self.checked_button,
            padding_y=-self.button_height,
            text="0",
            font=Font.REGULAR_BOLD_BIGGER,
            style=f"background: {Color.TRANS_WHITE}",
        )

        return result

    def get_availability_and_amount_label(self):
        # TODO нужно этот лейбл на низ
        result = Label(
            self.parent,
            width=self.button_three_quarters,
            height=self.button_height,
            right_from=self.weight_label,
            font=Font.REGULAR_ITALIC,
            text="Lorem ipsum",
            style=f"background: {Color.TRANS_WHITE}",
        )

        return result

    def get_stub(self):
        height = (
            self.image.height()
            + self.blocked_button.height()
            + self.title_button.height()
        )

        result = Label(
            self.parent,
            width=self.image.width(),
            height=height,
            move_to=self.image,
            style=f"background: {Color.GREY}",
            visible=False,
        )

        return result

    # === WORKERS ======================================================================

    def __set_blocked_status(self):
        self.__change_status(self.blocked_button, FoodCardState.BLOCKED)

    def __set_checked_status(self):
        self.__change_status(self.checked_button, FoodCardState.CHECKED)

    def __set_question_status(self):
        self.__change_status(self.question_button, FoodCardState.QUESTION)

    def __set_good_status(self):
        self.__change_status(self.good_button, FoodCardState.GOOD)

    def __change_status(self, small_btn, new_status):
        self.change_worker = ChangeFoodStatusWorker(self, small_btn, new_status)
        self.change_worker.start()
        self.change_worker.finished.connect(self.parent.get_food_from_db_worker.start)

    def __open_food_online(self):
        url = f"https://spb.vkusvill.ru{self.url}"
        webbrowser.open(url)


class SmallButton:
    CHECKED_BUTTON = "checked_button"
    GOOD_BUTTON = "good_button"
    QUESTION_BUTTON = "question_button"
    BLOCKED_BUTTON = "blocked_button"


SMALL_BUTTON_ATTRIBUTE_NAMES = [
    SmallButton.CHECKED_BUTTON,
    SmallButton.GOOD_BUTTON,
    SmallButton.QUESTION_BUTTON,
    SmallButton.BLOCKED_BUTTON,
]
