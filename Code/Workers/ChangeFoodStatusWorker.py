from functools import cached_property

from Code.TeverusSDK.DataBase import DataBase
from Code.Workers.BaseWorkers.BaseWorker import BaseWorker
from Code.constants import DATABASE, HIGHLIGHT, FoodCardState
from Code.helpers import current_datetime


class ChangeFoodStatusWorker(BaseWorker):
    def __init__(self, food_card, small_button, new_status):
        super(ChangeFoodStatusWorker, self).__init__()
        self.main = food_card.parent
        self.card = food_card
        self.small_button = small_button
        self.new_status = new_status

    def run(self):
        self.set_disabled(self.small_button, True)

        st = FoodCardState.UNSORTED if self.small_button_is_toggled else self.new_status
        self.set_new_status(st)

        self.set_disabled(self.small_button, False)

    # === PROPERTIES ===================================================================
    @cached_property
    def database(self):
        result = DataBase(DATABASE, default_table="states", default_column="url")
        return result

    @property
    def small_button_is_toggled(self):
        result = HIGHLIGHT in self.small_button.styleSheet()
        return result

    @property
    def food_card_is_present_in_db(self):
        result = self.database.find_in_db(
            column_name="url",
            target_value=self.card.url,
        )
        return result

    # === HELPERS ======================================================================
    def set_new_status(self, target_status):
        self.database.delete_in_db(find_by_value=self.card.url)

        values = [self.card.url, target_status, current_datetime()]
        self.database.write_to_db(new_values=values)
