import re
from functools import cached_property
from math import ceil

from PyQt5.QtGui import QPixmap

from Code.DTOs.FoodCard import FoodCard
from Code.TeverusSDK.DataBase import DataBase
from Code.Workers.BaseWorkers.BaseWorker import BaseWorker
from Code.constants import (
    DATABASE,
    Table,
    PER_PAGE,
    FOOD_CARDS_TITLE,
    Color,
    HIGHLIGHT,
    IMAGES_PATH,
    IMAGE_WIDTH,
    FoodCardState,
    CHOOSE_AT_LEAST_ONE_CATEGORY,
    NO_FOOD_IN_THIS_CATEGORY,
    Availability,
)
from Code.helpers import current_datetime


# noinspection PyAttributeOutsideInit
class GetFoodFromDBWorker(BaseWorker):
    def __init__(self, main):
        super().__init__()
        self.main = main
        self.index = None

    def run(self):
        self.database = DataBase(DATABASE)
        if not self.there_is_data_in_db:
            return
        elif not self.where_part:
            self.set_visible(self.main.stub_small, True)
            self.set_text(self.main.stub_small, CHOOSE_AT_LEAST_ONE_CATEGORY)
            return

        self.food_cards_all = self.get_food_cards_all()
        self.check_if_a_state_is_missing()
        self.food_cards_shown = self.get_food_cards_shown()
        if len(self.food_cards_shown) == 0:
            self.set_visible(self.main.stub_small, True)
            self.set_text(self.main.stub_small, NO_FOOD_IN_THIS_CATEGORY)
            return
        self.food_cards_hidden = self.get_food_cards_hidden()
        self.food_cards_this_page = self.get_food_cards_this_page()

        self.set_food_cards_header()
        self.set_food_cards()
        self.set_counter()
        self.set_pagination_buttons()

        self.set_visible(self.main.stub_small, False)
        self.set_visible(self.main.stub, False)

    # === PROPERTIES ===================================================================
    @cached_property
    def there_is_data_in_db(self):
        result = bool(self.database.read_table(Table.GENERAL_A))

        return result

    @property
    def where_part(self):
        toggled_btns = [b for b, t in self.main.categories_toggle.items() if t]
        states = [f"state = '{self.main.buttons_to_states[b]}'" for b in toggled_btns]
        with_or = " or ".join(states)
        result = f"where {with_or} " if with_or else ""

        return result

    # === HELPERS ======================================================================
    def get_food_cards_all(self):
        response = self.database.read_table(Table.GENERAL_A)
        result = [FoodCard(*e) for e in response]

        return result

    def get_food_cards_shown(self):
        sql_request = (
            f"select name, weight, price, ratio, g.url, amount, availability, state "
            "from general_a as g "
            "join states as s "
            "on g.url = s.url "
            f"{self.where_part}"
            "order by price desc"
        )
        response = self.database.execute_request(sql_request, fetch_all=True)
        result = [FoodCard(*e) for e in response]

        return result

    def get_food_cards_hidden(self):
        result = len(self.food_cards_all) - len(self.food_cards_shown)

        return result

    def get_food_cards_this_page(self):
        for _ in range(2):
            start = int(PER_PAGE * (self.main.current_page - 1))
            finish = int(PER_PAGE * self.main.current_page)
            result = self.food_cards_shown[start:finish]

            if result:
                break
            else:
                self.main.current_page -= 1

        return result

    def set_food_cards_header(self):
        shown = len(self.food_cards_shown)
        hidden = self.food_cards_hidden
        total = len(self.food_cards_all)
        shown_p = ceil(shown / total * 100)
        hidden_p = 100 - shown_p
        new_info = FOOD_CARDS_TITLE.format(
            shown=shown, hidden=hidden, total=total, shown_p=shown_p, hidden_p=hidden_p
        )
        self.set_text(self.main.food_cards_header, new_info)

    def set_food_cards(self):
        for self.index in range(PER_PAGE):
            try:
                self.food_card_info = self.food_cards_this_page[self.index]
                self.set_visible(self.main.food_cards[self.index].stub, False)
                self.set_ratio_value()
                self.set_ratio_color()
                self.set_weight_value()
                self.set_price_value()
                self.set_availability_and_amount_value()
                self.set_title_value()
                self.set_small_buttons_highlight()
                self.set_image()
                self.set_url()
            except IndexError:
                self.set_visible(self.main.food_cards[self.index].stub, True)

    def set_counter(self):
        self.max_page = ceil(len(self.food_cards_shown) / PER_PAGE) or 1
        adjust_width = len(str(self.max_page))
        current_page = str(self.main.current_page).rjust(adjust_width, "0")

        self.set_text(self.main.counter, f"{current_page}/{self.max_page}")

    def set_pagination_buttons(self):
        if self.max_page == 1:
            self.__set_buttons(False, False)
        elif self.main.current_page == 1:
            self.__set_buttons(False, True)
        elif self.main.current_page == self.max_page:
            self.__set_buttons(True, False)
        else:
            self.__set_buttons(True, True)

    def __set_buttons(self, button_left, button_right):
        self.set_visible(self.main.arrows_left_button, button_left)
        self.set_visible(self.main.arrows_right_button, button_right)

    def set_ratio_value(self):
        self.set_text(
            self.main.food_cards[self.index].ratio_label, str(self.food_card_info.ratio)
        )

    def set_ratio_color(self):
        self.ratios = self.get_ratios()
        self.ratio_group = self.get_ratio_group()
        ratio_to_color = {
            -2: Color.TRANS_RED,
            -1: Color.TRANS_ORANGE,
            0: Color.TRANS_YELLOW,
            1: Color.TRANS_LIGHT_GREEN,
            2: Color.TRANS_DARK_GREEN,
        }
        ratio_color = f"background: {ratio_to_color[self.ratio_group]}"
        self.set_style_sheet(self.main.food_cards[self.index].ratio_label, ratio_color)

    def get_ratios(self):
        sql_request = f"select distinct ratio from {Table.GENERAL_A} order by ratio asc"
        ratio_all = [e[0] for e in self.database.execute_request(sql_request)]
        ratio_batch = int(len(ratio_all) / 5)
        result = {}
        group = -2

        for ratio in ratio_all:
            if group not in result:
                result[group] = []
            if len(result[group]) < ratio_batch:
                result[group].append(ratio)
            else:
                if group != 2:
                    group += 1
                    result[group] = []
                result[group].append(ratio)

        return result

    def get_ratio_group(self):
        result = None

        for ratio_group, ratios in self.ratios.items():
            if self.food_card_info.ratio in ratios:
                result = ratio_group
                break

        return result

    def set_weight_value(self):
        self.set_text(
            self.main.food_cards[self.index].weight_label,
            str(self.food_card_info.weight),
        )

    def set_price_value(self):
        self.set_text(
            self.main.food_cards[self.index].price_label, str(self.food_card_info.price)
        )

    def set_title_value(self):
        new_name = self.get_name()
        self.set_text(self.main.food_cards[self.index].title_button, new_name)

    def get_name(self, length_line=17, total=57):
        result = self.food_card_info.name

        result = result[:-4] if result[-4:] == "рамм" else result

        if len(self.food_card_info.name) > length_line:
            string_split = result.split(" ")
            result = ""
            line_number = 1
            for index, element in enumerate(string_split):
                too_big = len(f"{result} {element}") >= (length_line * line_number)
                delimiter = " " if index else ""
                delimiter = "\n" if too_big else delimiter
                line_number = line_number + 1 if too_big else line_number
                result = f"{result}{delimiter}{element}"

        try:
            pattern = r",?\s?\n?\d{1,3}.?\n?к?г?м?л?ш?т?р?.?"
            weight = re.findall(pattern, result[-12:])[0]
            result = result.replace(weight, "").strip()
        except IndexError:
            pass

        result = result[:-5] if result[-5:] == ", вес" else result

        result = f"{result[:(total - 3)]}..." if len(result) > total else result

        if result.count("\n") > 2:
            new_result = "\n".join(result.split("\n")[:-1])
            result = f"{new_result}..."

        return result

    def set_small_buttons_highlight(self):
        food_card = self.main.food_cards[self.index]
        fc_state = self.food_card_info.state

        for attribute in self.main.category_button_to_small_button.values():
            self.__change_style(food_card, attribute, remove_highlight=True)

        if fc_state != FoodCardState.UNSORTED:
            button, attr = self.main.state_to_category_button_and_attribute[fc_state]
            proper_button_is_highlighted = HIGHLIGHT in button.styleSheet()
            if proper_button_is_highlighted:
                small_button = getattr(food_card, attr)
                self.set_style_sheet(small_button, HIGHLIGHT)

    def __change_style(
        self, food_card, attribute, add_highlight=False, remove_highlight=False
    ):
        small_button = getattr(food_card, attribute)
        style = small_button.styleSheet()
        if add_highlight:
            style_new = HIGHLIGHT if not style else f"{style}; {HIGHLIGHT}"
        elif remove_highlight:
            style_new = style.replace(HIGHLIGHT, "")
        else:
            style_new = ""

        self.set_style_sheet(small_button, style_new)

    def set_image(self):
        file_name = self.food_card_info.url.split("/")[-1].split(".")[0]
        file_path = IMAGES_PATH / f"{file_name}.webp"

        file_path = IMAGES_PATH / "a_stub.png" if not file_path.exists() else file_path

        pixmap = QPixmap(str(file_path))
        pixmap = pixmap.scaledToWidth(IMAGE_WIDTH, 1)
        self.set_pixmap(self.main.food_cards[self.index].image, pixmap)

    def set_url(self):
        self.main.food_cards[self.index].url = self.food_card_info.url

    def check_if_a_state_is_missing(self):
        states = self.database.read_table(Table.STATES)
        if len(self.food_cards_all) != len(states):
            urls_general = {e.url for e in self.food_cards_all}
            urls_states = {e[0] for e in states}
            missing_elements = urls_general - urls_states
            for missing_element in missing_elements:
                data = [missing_element, FoodCardState.UNSORTED, current_datetime()]
                self.database.write_to_db(data, Table.STATES)

    def set_availability_and_amount_value(self):
        data = f"{self.food_card_info.availability} {int(self.food_card_info.amount)}"
        widget = self.main.food_cards[self.index].availability_and_amount_label
        current_style = widget.styleSheet()

        colors = {Availability.TOMORROW: "purple", Availability.IN_STOCK: "green"}
        proper_color = colors[self.food_card_info.availability]
        new_style = f"{current_style}; color: {proper_color}"

        self.set_text(widget, data)
        self.set_style_sheet(widget, new_style)
