from Code.Workers.BaseWorkers.BaseWorker import BaseWorker
from Code.constants import HIGHLIGHT


class ShowCategoryWorker(BaseWorker):
    def __init__(self, main, button):
        super().__init__()
        self.main = main
        self.button = button
        self.button_style_current = self.button.styleSheet()

    def run(self):
        if self.button_is_toggled:
            button_style_new = self.button_style_current.replace(f"; {HIGHLIGHT}", "")
            new_toggle_state = False
        else:
            button_style_new = f"{self.button_style_current}; {HIGHLIGHT}"
            new_toggle_state = True

        self.set_style_sheet(self.button, button_style_new)
        self.main.categories_toggle[self.button] = new_toggle_state

    @property
    def button_is_toggled(self):
        return self.main.categories_toggle[self.button]
