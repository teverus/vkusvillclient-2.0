import re
from datetime import datetime
from functools import cached_property
from pathlib import Path
from time import sleep

from pandas import DataFrame
from playwright.sync_api import sync_playwright

from Code.DTOs.FoodCard import FoodCard
from Code.TeverusSDK.DataBase import DataBase
from Code.TeverusSDK.DataFrame import DataFrame as DataFrameT
from Code.TeverusSDK.web import WebHelpers
from Code.Widgets.BaseWidgets.MessageBox import MessageBoxIcon
from Code.Workers.BaseWorkers.BaseWorker import BaseWorker
from Code.constants import (
    HOST,
    XPath,
    HOST_WITH_PAGE,
    PAGINATION_XPATH,
    CARD_XPATH,
    NUMBER_OF_CARDS,
    STUB,
    IMAGE_XPATH,
    DATABASE,
    Table,
    FoodCardState,
    Availability,
)
from Code.helpers import current_datetime

PAGES_PROCESSED = "Pages processed"
PAGES_TIME = "Pages time"
DB_PROCESSED = "DB processed"
DB_TIME = "DB time"


# noinspection PyAttributeOutsideInit
class SyncFoodWorker(BaseWorker):
    def __init__(self, main, headless=True):
        super().__init__()
        self.main = main
        self.headless = headless
        self.run_stats = {PAGES_PROCESSED: 0, PAGES_TIME: 0, DB_TIME: 0}

    def run(self):
        if not self.main.address:
            self.show_error_message_box()
            return

        self.database = DataBase(DATABASE)
        self.stub = self.main.stub

        self.set_visible(self.stub, True)
        self.set_text(self.stub, "Подключаемся к сайту ВкусВилл\n")

        self.get_food_cards()
        self.write_food_cards_to_database()

        self.set_visible(self.stub, False)
        self.show_stats_message_box()

    # === PROPERTIES ===================================================================

    @cached_property
    def page(self):
        self.set_text(self.stub, "Запускаем браузер в фоне\n")
        browser = self.playwright.firefox.launch(headless=self.headless)
        context = browser.new_context()
        context.grant_permissions(["geolocation"])
        page = context.new_page()
        page.set_viewport_size({"width": 1920, "height": 1080})

        return page

    @cached_property
    def max_page(self):
        paginator = self.page.locator(PAGINATION_XPATH).first
        result = int(paginator.inner_text().split(" ")[-2])
        # result = 1

        return result

    # === HELPERS ======================================================================

    def show_error_message_box(self):
        self.set_text(self.main.message_box_info, "Вам следует заполнить адрес!")
        self.set_icon_int(self.main.message_box_info, MessageBoxIcon.ERROR)
        self.exec_(self.main.message_box_info)

    def get_food_cards(self):
        time_start = datetime.now()
        self.food_cards = []
        self.reached_the_end_of_available_food = False

        with sync_playwright() as self.playwright:
            self.page.goto(HOST)

            self.set_proper_address()

            for page_number in range(1, self.max_page + 1):
                self.stats = f"{page_number:02}/{self.max_page:02}"
                message = f"{self.stats}\nОткрываем страницу\n"
                self.set_text(self.stub, message)
                self.page.goto(HOST_WITH_PAGE.format(page_number))
                self.set_text(self.stub, f"{self.stats}\nПолучаем данные\n")
                self.page.wait_for_load_state("load")
                sleep(0.3)
                self.get_food_cards_on_a_page()
                self.food_cards += self.cards_on_the_page
                if self.reached_the_end_of_available_food:
                    break

        time_finish = datetime.now()
        delta = time_finish - time_start
        self.run_stats[PAGES_PROCESSED] = page_number
        self.run_stats[PAGES_TIME] = delta

    def set_proper_address(self):
        self.set_text(self.stub, "Настраиваем нужный адрес\n")

        address_input = self.page.locator(XPath.ADDRESS_INPUT).nth(0)
        steps = [
            self.page.locator(XPath.CHOOSE_YOUR_LOCATION).click,
            address_input.click,
            address_input.clear,
            (address_input.type, self.main.address),
            self.page.locator(XPath.ADDRESSES_SUGGESTED).nth(0).click,
            self.page.locator(XPath.ADDRESS_CONFIRM).click,
        ]
        for step in steps:
            step[0](step[1]) if isinstance(step, tuple) else step()
            sleep(0.5)
            self.set_text(self.stub, f"{self.stub.text()}.")

        for _ in range(10):
            if self.page.locator(XPath.CLOSE_BUTTON).count() == 1:
                break
            sleep(1)
        self.page.locator(XPath.CLOSE_BUTTON).click()
        self.page.wait_for_load_state("networkidle")

        address_now = self.page.locator(XPath.ADDRESS_LABEL).nth(0).inner_text()
        for element in self.main.address.split():
            try:
                assert element in address_now
            except Exception:
                raise Exception(
                    f"Cound't find '{self.main.address}' in '{address_now}'"
                )
        ...

    def get_food_cards_on_a_page(self):
        result = []
        self.cards = self.page.locator(CARD_XPATH)
        self.cards_number = self.cards.count()

        for index in range(NUMBER_OF_CARDS):
            self.set_text(self.stub, f"{self.stub.text()}.")
            self.card = self.cards.nth(index)
            self.card_text = self.card.all_inner_texts()[0].split("\n")
            self.card_url = self.card.locator("//a").first.get_attribute("href")
            self.get_image_if_needed(index)

            food_card = self.get_food_card()

            if food_card.availability == Availability.OUT_OF_STOCK:
                self.reached_the_end_of_available_food = True
                break

            result.append(food_card)

        self.cards_on_the_page = result

    def get_image_if_needed(self, index):
        card_url = self.card_url.split("/")[-1].split(".")[0]
        image_path = Path("Files/Images") / Path(f"{card_url}.webp")
        if not image_path.exists():
            self.card_image_url = self.get_card_image_url(index)
            if self.card_image_url != STUB:
                WebHelpers.save_from_web(self.card_image_url, image_path)

    def get_card_image_url(self, index):
        self.card.hover()
        card_image = self.page.locator(IMAGE_XPATH).nth(index)
        result = card_image.get_attribute("src")
        result = STUB if "site_MiniWebP" not in result else result

        return result

    def get_food_card(self):
        weight = self.get_weight()
        price = int(self.card_text[2].replace("\xa0", ""))
        state = self.get_state()
        amount = self.get_available_amount()
        availability = self.get_availability()

        result = FoodCard(
            name=self.card_text[0].replace("\xa0", " "),
            weight=weight,
            price=price,
            ratio=round(weight / price, 2),
            url=self.card_url,
            state=state,
            amount=amount,
            availability=availability,
        )

        return result

    def get_weight(self):
        multiplier = 1
        weight_indication = self.card_text[1]
        digits = [int(e) for e in re.findall(r"\d*", weight_indication) if e]

        if len(digits) == 1:
            if "кг" in weight_indication:
                multiplier = 1000
            else:
                multiplier = 1
            weight = digits[0]
        elif len(digits) == 2:
            if "/" in weight_indication:
                weight = sum(digits)
            elif "кг" in weight_indication:
                kilograms = digits[0] * 1000
                grams = digits[1] * 10
                weight = kilograms + grams
            else:
                raise Exception(f"{weight_indication = }\n{digits = }")
        else:
            raise Exception(f"{weight_indication = }\n{digits = }")

        if weight and multiplier:
            result = weight * multiplier
            return result
        else:
            raise Exception(f"{weight = }\n{multiplier = }\n{weight_indication}")

    def get_state(self):
        result = self.database.find_in_db(
            target_table=Table.STATES,
            column_name="url",
            target_value=self.card_url,
        )
        if not result:
            state = FoodCardState.UNSORTED
            self.database.write_to_db(
                new_values=[self.card_url, state, current_datetime()],
                target_table=Table.STATES,
            )
        else:
            state = result[0][1]

        return state

    def get_available_amount(self):
        out_of_stock = Availability.OUT_OF_STOCK in str(self.card_text)
        r = 0 if out_of_stock else self.card_text[-1].split()[-2]

        try:
            result = int(r)
        except ValueError:
            result = round(float(r), 2)

        return result

    def get_availability(self):
        result = [state for state in Availability.all if state in self.card_text[-1]][0]

        return result

    def write_food_cards_to_database(self):
        time_start = datetime.now()

        self.write_food_cards_to_db_with_pandas()

        time_finish = datetime.now()
        delta = time_finish - time_start
        self.run_stats[DB_PROCESSED] = len(self.food_cards)
        self.run_stats[DB_TIME] = delta

    def show_stats_message_box(self):
        pages_time = str(self.run_stats[PAGES_TIME]).split(".")[0]
        db_time = str(self.run_stats[DB_TIME]).split(".")[0]
        total = str(self.run_stats[PAGES_TIME] + self.run_stats[DB_TIME]).split(".")[0]
        message = (
            f"[{pages_time}] Открыто страниц: {self.run_stats[PAGES_PROCESSED]}\n"
            f"[{db_time}] Добавлено блюд: {self.run_stats[DB_PROCESSED]}\n"
            f"[{total}] Затрачено времени"
        )
        self.set_text(self.main.message_box_info, message)
        self.set_icon_int(self.main.message_box_info, MessageBoxIcon.INFORMATION)
        self.exec_(self.main.message_box_info)

    def write_food_cards_to_db_with_pandas(self):
        df = DataFrameT.read_from_sql(Table.GENERAL_A, self.database.connection)
        result = DataFrame([], columns=list(df.columns))
        for i, f in enumerate(self.food_cards):
            data = [f.name, f.weight, f.price, f.ratio, f.url, f.amount, f.availability]
            result.loc[i] = data
        con = self.database.connection
        result.to_sql(Table.GENERAL_A, con, if_exists="replace", index=False)
