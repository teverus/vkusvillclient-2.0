# === BACK CONSTANTS ===================================================================
from enum import Enum
from pathlib import Path

HOST = "https://spb.vkusvill.ru/goods/gotovaya-eda/"
HOST_WITH_PAGE = "https://spb.vkusvill.ru/goods/gotovaya-eda/?PAGEN_1={}"
PAGINATION_XPATH = "//*[@class='VV_Pager js-lk-pager']"
CARD_XPATH = "//*[contains(@class, 'ProductCard__content')]"
IMAGE_XPATH = "//*[contains(@class, 'ProductCard__imageImg')]"
DATABASE = "Files/database.db"
IMAGES_PATH = Path("Files/Images")

# === GUI CONSTANTS ====================================================================
WINDOW_WIDTH = 1616
WINDOW_HEIGHT = 1016
PADDING_BIG = 8
PADDING_SMALL = int(PADDING_BIG / 2)
BUTTON_WIDTH = int(WINDOW_WIDTH / 4) - int(PADDING_BIG * 1.5)
BUTTON_WIDTH_LONG = int(WINDOW_WIDTH * 0.75) - int(PADDING_BIG * 1.5)
BUTTON_HEIGHT = int((WINDOW_HEIGHT - int(PADDING_BIG * 2)) / 20)
ICON_SIZE = 36
PER_PAGE = 12
IMAGE_WIDTH = int(BUTTON_WIDTH_LONG / 4) - 3
FOOD_CARDS_TITLE = (
    "Показано: {shown} [{shown_p}%]| Скрыто: {hidden} [{hidden_p}%]| Всего: {total}"
)
HIGHLIGHT = "background: SkyBlue"
STUB = "stub"
NUMBER_OF_CARDS = 24
CHOOSE_AT_LEAST_ONE_CATEGORY = "Выберите хотя бы одну из категорий:\nПроверенные\nВкусные\nПод вопросом\nНе вкусные\nНеразобранные"
NO_FOOD_IN_THIS_CATEGORY = (
    "В данной категории нет блюд.\nПожалуйста, выберите другую категорию."
)


# === ENUMS ============================================================================
class Availability:
    IN_STOCK = "В наличии"
    TOMORROW = "Завтра будет"
    OUT_OF_STOCK = "Не осталось"

    @classmethod
    @property
    def all(cls):
        result = [
            Availability.IN_STOCK,
            Availability.TOMORROW,
            Availability.OUT_OF_STOCK,
        ]

        return result


class Table:
    GENERAL_A = "general_a"
    STATES = "states"
    SETTINGS = "settings"


class FoodTypeNames(Enum):
    FOOD_CURRENT = "Текущие блюда"
    FOOD_ALL = "Все блюда"


# === QUASI-ENUMS ======================================================================


class XPath:
    CHOOSE_YOUR_LOCATION = (
        "//*[@class='HeaderATDToggler__Text rtext _desktop-md _tablet-sm _mobile-sm']"
    )
    ADDRESS_INPUT = "//textarea"
    ADDRESSES_SUGGESTED = "//*[@class='VV_Dropdown__option js-suggest']"
    ADDRESS_CONFIRM = "//*[text()='Доставить сюда']"
    GOT_IT = "//*[text()=' Понятно ']"
    CLOSE_BUTTON = "//*[@class='VV_ModalCloser']"
    ADDRESS_LABEL = "//*[@class='HeaderATDToggler__TextInner']"


class Color:
    TRANS_WHITE = "rgba(255,255,255, 0.75)"
    TRANS_GREEN = "rgba(144, 238, 144, 0.75)"
    GREY = "rgb(240, 240, 240)"
    TRANS_RED = "rgba(255, 0, 0, 0.75)"
    TRANS_ORANGE = "rgba(255, 165, 0, 0.75)"
    TRANS_YELLOW = "rgba(255, 255, 0, 0.75)"
    TRANS_LIGHT_GREEN = "rgba(144, 238, 144, 0.75)"
    TRANS_DARK_GREEN = "rgba(0, 128, 0, 0.75)"


class FoodCardState:
    BLOCKED = "Blocked"
    CHECKED = "Checked"
    QUESTION = "Question"
    GOOD = "Good"
    UNSORTED = "Unsorted"


FOOD_CARD_STATES = [
    FoodCardState.CHECKED,
    FoodCardState.GOOD,
    FoodCardState.QUESTION,
    FoodCardState.BLOCKED,
]


class Icon:
    SHOW = "Files/Icons/show.png"
    HIDE = "Files/Icons/hide.png"
    CHECKED = "Files/Icons/checked.png"
    QUESTION = "Files/Icons/question.png"
    THUMBS_UP = "Files/Icons/thumbs_up.png"
    THUMBS_DOWN = "Files/Icons/thumbs_down.png"


class Settings:
    ADDRESS = "address"
