from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QStyleFactory, QWidget

from Code.TeverusSDK.DataBase import DataBase
from Code.Widgets.BaseWidgets.BaseWidget import Font
from Code.Widgets.BaseWidgets.Button import Button
from Code.Widgets.BaseWidgets.ComboBox import ComboBox
from Code.Widgets.BaseWidgets.InputBox import InputBox
from Code.Widgets.BaseWidgets.Label import Label
from Code.Widgets.BaseWidgets.MessageBox import (
    MessageBox,
    MessageBoxIcon,
    MessageBoxButton,
    MessageBoxButtonRole,
)
from Code.Widgets.FoodCard import FoodCard, SmallButton
from Code.Workers.GetFoodFromDBWorker import GetFoodFromDBWorker
from Code.Workers.ShowCategoryWorker import ShowCategoryWorker
from Code.Workers.SyncFoodWorker import SyncFoodWorker
from Code.constants import (
    WINDOW_WIDTH,
    WINDOW_HEIGHT,
    PADDING_BIG,
    BUTTON_WIDTH,
    BUTTON_HEIGHT,
    BUTTON_WIDTH_LONG,
    PADDING_SMALL,
    FOOD_CARDS_TITLE,
    Color,
    FoodCardState,
    Icon,
    HIGHLIGHT,
    Settings,
    DATABASE,
    Table,
)


# noinspection PyAttributeOutsideInit
class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        # --- Variables ----------------------------------------------------------------
        self.window_title = "Доставка ВкусВилл"
        self.window_icon = "Files/Icons/favicon.png"
        self.database = DataBase(DATABASE)
        self.settings = dict([e for e in self.database.read_table(Table.SETTINGS)])
        self.address = self.settings[Settings.ADDRESS] or ""
        self.current_page = 1

        # --- Workers ------------------------------------------------------------------
        self.sync_food_worker = SyncFoodWorker(self)
        self.get_food_from_db_worker = GetFoodFromDBWorker(self)

        # --- Widgets ------------------------------------------------------------------
        self.left_top_level()
        self.left_center_level()
        self.left_bottom_level()

        self.right_top_level()
        self.right_center_level()
        self.right_bottom_level()

        # --- Meta-widgets -------------------------------------------------------------
        self.stub_level()
        self.message_box_level()
        self.set_window()

        # --- Initial instructions -----------------------------------------------------
        self.get_food_from_db_worker.start()

    # === HELPERS ======================================================================

    def left_bottom_level(self):
        self.address_input = InputBox(
            self,
            width=BUTTON_WIDTH - int(BUTTON_HEIGHT * 2) - int(PADDING_SMALL * 2),
            height=BUTTON_HEIGHT,
            under=self.category_unsorted_button,
            padding_y=PADDING_SMALL,
            style="padding-left: 10px",
            placeholder="Введите свой адрес",
            text_changed=self.set_address,
            text=self.address,
        )

        self.address_ok_button = Button(
            self,
            width=BUTTON_HEIGHT,
            height=BUTTON_HEIGHT,
            right_from=self.address_input,
            padding_x=PADDING_SMALL,
            icon="Files/Icons/save.png",
            disabled=True,
            clicked=self.save_address,
        )

        self.address_clear_button = Button(
            self,
            width=BUTTON_HEIGHT,
            height=BUTTON_HEIGHT,
            right_from=self.address_ok_button,
            padding_x=PADDING_SMALL,
            icon="Files/Icons/clear.png",
            clicked=self.delete_address,
        )

        self.sync_with_website_button = Button(
            self,
            width=BUTTON_WIDTH,
            height=int(BUTTON_HEIGHT * 2),
            under=self.address_input,
            padding_y=PADDING_SMALL,
            text="Синхронизировать с сайтом",
            icon="Files/Icons/sync.png",
            style="background: LightGreen",
            clicked=self.sync_food,
        )

    def set_window(self):
        self.setWindowTitle(self.window_title)
        self.setWindowIcon(QIcon(self.window_icon))
        self.setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT)
        self.show()

    def left_top_level(self):
        self.parameters_label = Label(
            self,
            BUTTON_WIDTH,
            BUTTON_HEIGHT,
            padding_x=PADDING_BIG,
            padding_y=PADDING_BIG,
            text="Параметры",
            font=Font.TITLE,
        )

    def right_top_level(self):
        self.food_cards_header = Label(
            self,
            BUTTON_WIDTH_LONG,
            BUTTON_HEIGHT,
            right_from=self.parameters_label,
            padding_x=PADDING_BIG,
            text=FOOD_CARDS_TITLE.format(
                shown=0, hidden=0, total=0, shown_p=0, hidden_p=0
            ),
            font=Font.TITLE,
        )

    def right_center_level(self):
        self.food_card_1 = FoodCard(
            self,
            under=self.food_cards_header,
        )

        self.food_card_2 = FoodCard(
            self,
            right_from=self.food_card_1.image,
            padding_x=PADDING_SMALL,
        )

        self.food_card_3 = FoodCard(
            self,
            right_from=self.food_card_2.image,
            padding_x=PADDING_SMALL,
        )

        self.food_card_4 = FoodCard(
            self,
            right_from=self.food_card_3.image,
            padding_x=PADDING_SMALL,
        )

        self.food_card_5 = FoodCard(
            self,
            under=self.food_card_1.title_button,
            padding_y=PADDING_SMALL,
        )

        self.food_card_6 = FoodCard(
            self,
            right_from=self.food_card_5.image,
            padding_x=PADDING_SMALL,
        )

        self.food_card_7 = FoodCard(
            self,
            right_from=self.food_card_6.image,
            padding_x=PADDING_SMALL,
        )

        self.food_card_8 = FoodCard(
            self,
            right_from=self.food_card_7.image,
            padding_x=PADDING_SMALL,
        )

        self.food_card_9 = FoodCard(
            self,
            under=self.food_card_5.title_button,
            padding_y=PADDING_SMALL,
        )

        self.food_card_10 = FoodCard(
            self,
            right_from=self.food_card_9.image,
            padding_x=PADDING_SMALL,
        )

        self.food_card_11 = FoodCard(
            self,
            right_from=self.food_card_10.image,
            padding_x=PADDING_SMALL,
        )

        self.food_card_12 = FoodCard(
            self,
            right_from=self.food_card_11.image,
            padding_x=PADDING_SMALL,
        )

        self.food_cards = [
            self.food_card_1,
            self.food_card_2,
            self.food_card_3,
            self.food_card_4,
            self.food_card_5,
            self.food_card_6,
            self.food_card_7,
            self.food_card_8,
            self.food_card_9,
            self.food_card_10,
            self.food_card_11,
            self.food_card_12,
        ]

    def right_bottom_level(self):
        arrows_width = int(BUTTON_WIDTH_LONG / 2) - int(BUTTON_HEIGHT / 1.5) + 2
        arrows_height = BUTTON_HEIGHT

        self.arrows_left_button = Button(
            self,
            width=arrows_width,
            height=arrows_height,
            under=self.food_card_9.title_button,
            padding_y=PADDING_SMALL,
            text="<<",
            clicked=self.show_prev_page,
        )

        self.counter = Label(
            self,
            width=int(arrows_height * 1.25),
            height=arrows_height,
            right_from=self.arrows_left_button,
            text="1/1",
        )

        self.arrows_right_button = Button(
            self,
            width=arrows_width,
            height=arrows_height,
            right_from=self.counter,
            text=">>",
            clicked=self.show_next_page,
        )

    def sort_by_level(self):
        self.sort_by_label = Label(
            self,
            width=int(BUTTON_WIDTH / 2) - int(BUTTON_HEIGHT / 2),
            height=BUTTON_HEIGHT,
            under=self.parameters_label,
            text="Сортировать по",
            disabled=True,
        )

        self.sort_by_value_combobox = ComboBox(
            self,
            width=int(BUTTON_WIDTH / 2) - int(BUTTON_HEIGHT / 2),
            height=BUTTON_HEIGHT,
            right_from=self.sort_by_label,
            items=[" цене", " весу", " соотношению"],
            disabled=True,
        )

        self.sort_by_order_combobox = ComboBox(
            self,
            width=BUTTON_HEIGHT,
            height=BUTTON_HEIGHT,
            right_from=self.sort_by_value_combobox,
            items=["  ↓", "  ↑"],
            disabled=True,
        )

    def get_tbd_button(self, btn_number, pad_number):
        return Label(
            self,
            width=BUTTON_WIDTH,
            height=int(BUTTON_HEIGHT * btn_number) - int(PADDING_SMALL * pad_number),
            under=self.sort_by_label,
            padding_y=PADDING_SMALL,
            text="TBD",
            disabled=True,
        )

    def left_center_level(self):
        self.sort_by_level()
        self.tbd_button = self.get_tbd_button(12, 5)
        self.category_buttons_level()

    def stub_level(self):
        self.stub_small = Label(
            self,
            width=BUTTON_WIDTH_LONG,
            height=WINDOW_HEIGHT - int(PADDING_BIG * 2),
            right_from=self.parameters_label,
            padding_x=PADDING_BIG,
            text='Введите адрес и нажмите "Синхронизировать с сайтом"',
            style=f"background: {Color.GREY}",
        )

        self.stub = Label(
            self,
            width=WINDOW_WIDTH - (PADDING_BIG * 2),
            height=WINDOW_HEIGHT - (PADDING_BIG * 2),
            padding_x=PADDING_BIG,
            padding_y=PADDING_BIG,
            text="",
            style=f"background: {Color.GREY}",
            visible=False,
        )

    def category_buttons_level(self):
        half = int(BUTTON_WIDTH / 2) - int(PADDING_SMALL / 2)
        style = "text-align: left; padding-left: 5 px"

        self.category_checked_button = Button(
            self,
            width=half,
            height=BUTTON_HEIGHT,
            under=self.tbd_button,
            text="Проверенные",
            icon=Icon.CHECKED,
            style=f"{style}; {HIGHLIGHT}",
            clicked=self.show_category_checked,
        )
        self.category_good_button = Button(
            self,
            width=half,
            height=BUTTON_HEIGHT,
            right_from=self.category_checked_button,
            padding_x=PADDING_SMALL,
            text="Вкусные",
            style=f"{style}; {HIGHLIGHT}",
            icon=Icon.THUMBS_UP,
            clicked=self.show_category_good,
        )
        self.category_question_button = Button(
            self,
            width=half,
            height=BUTTON_HEIGHT,
            under=self.category_checked_button,
            padding_y=PADDING_SMALL,
            text="Под вопросом",
            style=f"{style}; {HIGHLIGHT}",
            icon=Icon.QUESTION,
            clicked=self.show_category_question,
        )
        self.category_blocked_button = Button(
            self,
            width=half,
            height=BUTTON_HEIGHT,
            right_from=self.category_question_button,
            padding_x=PADDING_SMALL,
            text="Не вкусные",
            style="text-align: left; padding-left: 5 px",
            icon=Icon.THUMBS_DOWN,
            clicked=self.show_category_blocked,
        )

        self.category_unsorted_button = Button(
            self,
            width=BUTTON_WIDTH,
            height=BUTTON_HEIGHT,
            under=self.category_question_button,
            padding_y=PADDING_SMALL,
            text="Неразобранные",
            style=f"text-align: left; padding-left: 85 px; {HIGHLIGHT}",
            icon="Files/Icons/doubt.png",
            clicked=self.show_category_unsorted,
        )

        # --- Mappings -----------------------------------------------------------------

        self.categories_toggle = {
            self.category_checked_button: True,
            self.category_good_button: True,
            self.category_question_button: True,
            self.category_blocked_button: False,
            self.category_unsorted_button: True,
        }
        self.buttons_to_states = {
            self.category_checked_button: FoodCardState.CHECKED,
            self.category_good_button: FoodCardState.GOOD,
            self.category_question_button: FoodCardState.QUESTION,
            self.category_blocked_button: FoodCardState.BLOCKED,
            self.category_unsorted_button: FoodCardState.UNSORTED,
        }
        self.category_button_to_small_button = {
            self.category_checked_button: SmallButton.CHECKED_BUTTON,
            self.category_good_button: SmallButton.GOOD_BUTTON,
            self.category_question_button: SmallButton.QUESTION_BUTTON,
            self.category_blocked_button: SmallButton.BLOCKED_BUTTON,
        }
        self.state_to_category_button_and_attribute = {
            FoodCardState.CHECKED: [
                self.category_checked_button,
                SmallButton.CHECKED_BUTTON,
            ],
            FoodCardState.GOOD: [
                self.category_good_button,
                SmallButton.GOOD_BUTTON,
            ],
            FoodCardState.QUESTION: [
                self.category_question_button,
                SmallButton.QUESTION_BUTTON,
            ],
            FoodCardState.BLOCKED: [
                self.category_blocked_button,
                SmallButton.BLOCKED_BUTTON,
            ],
        }

    def message_box_level(self):
        self.message_box_disclaimer = MessageBox(
            window_title=self.window_title,
            text="Обновление всех данных может занять много времени.\nВы точно хотите начать?",
            icon=MessageBoxIcon.QUESTION,
            buttons=[
                MessageBoxButton("Запустить", MessageBoxButtonRole.YES),
                MessageBoxButton("Назад", MessageBoxButtonRole.NO),
            ],
        )

        self.message_box_info = MessageBox(
            window_title=self.window_title,
            text="Hello world",
            icon=MessageBoxIcon.INFORMATION,
        )

    # === WORKERS ======================================================================

    def sync_food(self):
        self.sync_food_worker = SyncFoodWorker(self)
        self.sync_food_worker.start()
        self.sync_food_worker.finished.connect(self.get_food_from_db_worker.start)

    def show_prev_page(self):
        self.current_page -= 1
        self.get_food_from_db_worker.start()

    def show_next_page(self):
        self.current_page += 1
        self.get_food_from_db_worker.start()

    def show_category_checked(self):
        self.__show_category(self.category_checked_button)

    def show_category_good(self):
        self.__show_category(self.category_good_button)

    def show_category_question(self):
        self.__show_category(self.category_question_button)

    def show_category_blocked(self):
        self.__show_category(self.category_blocked_button)

    def show_category_unsorted(self):
        self.__show_category(self.category_unsorted_button)

    def __show_category(self, button):
        self.current_page = 1
        self.show_category_worker = ShowCategoryWorker(self, button)
        self.show_category_worker.start()
        self.show_category_worker.finished.connect(self.get_food_from_db_worker.start)

    # === PROPERTIES ===================================================================

    # === HELPERS ======================================================================
    def set_address(self, new_address):
        if new_address != self.address:
            self.address_ok_button.setDisabled(False)

        self.address = new_address

    def save_address(self):
        self.database.update_value(
            target_table=Table.SETTINGS,
            target_column="value",
            new_value=self.address,
            search_by_column="name",
            search_by_name=Settings.ADDRESS,
        )
        self.address_ok_button.setDisabled(True)

    def delete_address(self):
        self.database.update_value(
            target_table=Table.SETTINGS,
            target_column="value",
            new_value="",
            search_by_column="name",
            search_by_name=Settings.ADDRESS,
        )
        self.address = None
        self.address_input.setText("")


if __name__ == "__main__":
    application = QApplication([])
    application.setStyle(QStyleFactory.create("Fusion"))
    window = MainWindow()
    application.exec_()
